from pathlib import Path
from math import atan2, sqrt
from typing import Dict, Set, Iterable, Callable, Union
from abc import ABC, abstractmethod
import logging
from threading import local
import csv

import numpy as np
from scipy.optimize import root, root_scalar

from xWASP_CN.errors import InvalidInputError, ConvergenceError, MissingParameter, OperationError
from xWASP_CN.yaml_helpers import get_yaml, get_modified_yaml, check_yaml_for_forces, parse_angle_unit, get_as_radians, first, get_initial_attitude
import xWASP_CN.xdyn_gRPC as xdyn
from xWASP_CN.data_structures import Parameters, Mode, UnitValue, StaticResults, ForceLabel
from xWASP_CN.loggers import ParametersLoggerAdapter, log_stack

#import matplotlib.pyplot as plt

local = local()
local.logger = logging.getLogger('xWASP_CN')

class StaticsComputer(ABC):
    """This class is an interface for the different static solution algorithms."""
    def __init__(self, config: dict, input_file, mode: Mode, engine_commands: Set[str] = None, steering_commands: Set[str] = None, ship_speed: float = None, wind_model: dict = None):
        self.yaml_input =  get_yaml(input_file)
        self.input_path = Path(input_file).parent
        self.ship_name = str(self.yaml_input['bodies'][0]['name'])
        self.mode = mode
        self.ship_speed = ship_speed
        if self.mode == Mode.PPP and self.ship_speed is None:
            raise InvalidInputError('ship_speed', ship_speed, "A ship speed must be provided in PPP mode.")
        self.wind_model = wind_model

        self.steering_commands = steering_commands
        self.max_steering_command = None
        if self.steering_commands is not None:
            self.max_steering_command = float(config['max steering command'])

        self.engine_commands = engine_commands
        self.max_engine_command = None
        if self.engine_commands is not None:
            self.max_engine_command = float(config['max engine command'])

        if self.mode is Mode.PPP and self.engine_commands is None:
            local.logger.info("In the absence of an engine command in PPP mode, the propulsion axis will be ignored when solving.")

        self.solve_hydrostatics = True
        if 'solve hydrostatics' in config:
            self.solve_hydrostatics = bool(config['solve hydrostatics'])
        else:
            local.logger.info("Solving hydrostatics by default.")

        self.max_speed = 100.
        if 'max ship speed' in config:
            self.max_speed = abs(float(config['max ship speed']))
        else:
            local.logger.info("Defaulting max ship speed to %f m/s.", self.max_speed)
        self.max_heel = np.radians(30.)
        if 'max heel' in config:
            self.max_heel = get_as_radians(config['max heel'])
        else:
            local.logger.info("Defaulting max heel to %f rad.", self.max_heel)
        self.max_trim = np.radians(30.)
        if 'max trim' in config:
            self.max_trim = get_as_radians(config['max trim'])
        else:
            local.logger.info("Defaulting max trim to %f rad.", self.max_trim)
        self.max_sinkage = 10.
        if 'max sinkage' in config:
            self.max_sinkage = float(config['max sinkage'])
        else:
            local.logger.info("Defaulting max sinkage to %f m.", self.max_sinkage)

        self.save_convergence_data = False
        if 'save convergence data' in config:
            self.save_convergence_data = bool(config['save convergence data'])

    @abstractmethod
    def compute_statics(self, parameters: Parameters, x0: Dict[str, float] = None) -> StaticResults:
        """This function computes the steady state given a set of parameters."""

class DecoupledRootFindingStaticsComputer(StaticsComputer):
    """This class uses xdyn-for-me to compute the steady state using a de-coupled root-finding approach."""
    def __init__(self, config: dict, input_file, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, ship_speed: float = None, wind_model: dict = None):
        super().__init__(config, input_file, mode, engine_commands, steering_commands, ship_speed, wind_model)
        self.config = {}
        if 'tolerance' in config:
            self.tolerance = float(config['tolerance'])
        else:
            self.tolerance = 10e-10
            local.logger.info("Defaulting tolerance to %s.", self.tolerance)
        if 'max inner iterations' in config:
            self.maxiter = int(config['max inner iterations'])
        else:
            self.maxiter = 300
            local.logger.info("Defaulting max inner (scalar root-finding) iterations to %s.", self.maxiter)

        if 'max decoupled iterations' in config:
            self.max_decoupled_iterations = config['max decoupled iterations']
        else:
            self.max_decoupled_iterations = 3
            local.logger.info("Defaulting max decoupled iterations to %s.", self.max_decoupled_iterations)

        if 'decoupled root-finding algorithm' in config:
            self.scalar_algorithm = config['decoupled root-finding algorithm']
        else:
            self.scalar_algorithm = 'toms748'
            local.logger.info("Defaulting decoupled root-finding algorithm to %s.", self.scalar_algorithm)

        if 'coupled root-finding algorithm' in config:
            self.coupled_algorithm = config['coupled root-finding algorithm']
        else:
            self.coupled_algorithm = 'hybr'
            local.logger.info("Defaulting coupled root-finding algorithm to %s.", self.coupled_algorithm)

        if self.mode is Mode.VPP and self.engine_commands is not None:
            local.logger.warning('An engine command was provided in VPP mode, but it will be ignored because it is not an unknown in this case. If you wish to apply a fixed engine command, set it in the model file.')

        self.always_save_forces_debug_file = False
        if 'always save scalar root-finding debug file' in config:
            self.always_save_forces_debug_file = bool(config['always save scalar root-finding debug file'])
        if self.always_save_forces_debug_file:
            local.logger.warning("Debug files will be generated for all scalar root-finding procedures. This may generate a large number of files.")

    @log_stack(local, 'initial hydrostatics')
    def _compute_initial_hydrostatics(self, x0=None) ->  Dict[str, float]:
        mode_backup = self.mode
        self.mode = Mode.HS
        try:
            parameters = Parameters(0., 0.)
            yaml_input = get_modified_yaml(self.yaml_input, parameters)
            x = self._get_forced_states(x0)
            with xdyn.Xdyn_server_steady(yaml_input, self.config, self.input_path) as model:
                x.update(self._solve_hydrostatics(model, parameters, x))
                states = model.get_system_states(x)
        except ConvergenceError as error:
            error.parameters = "initial hydrostatics"
            raise error
        finally:
            self.mode = mode_backup
        return StaticResults(states)

    def compute_statics(self, parameters: Parameters = None, x0: Dict[str, float] = None) -> StaticResults:
        if parameters is None: # Compute hydrostatics
            return self._compute_initial_hydrostatics(x0)
        else:
            yaml_input = get_modified_yaml(self.yaml_input, parameters, wind_model=self.wind_model)
            if x0 is None:
                x = self._get_forced_states()
            else:
                x = x0.copy()
                if 'u' in x: del x['u']
                if 'v' in x: del x['v']
                if 'w' in x: del x['w']
            with xdyn.Xdyn_server_steady(yaml_input, self.config, self.input_path) as model:
                if self.solve_hydrostatics:
                    x.update(self._solve_problem(model, parameters, [self._solve_horizontal_problem, self._solve_hydrostatics], self._solve_coupled_problem, x))
                else:
                    x.update(self._solve_horizontal_problem(model, parameters, x))
                states = model.get_system_states(x)
            return StaticResults(states)

    def _get_forced_states(self, x0=None, solving_for=None):
        if x0 is None:
            x0 = {}
        if solving_for is None:
            solving_for = []
        # Default values that may be overwritten by x0
        forced_states = {'dx_dt': 0.,
                         'dy_dt': 0.,
                         'z': 0.,
                         'phi': 0.,
                         'theta': 0.}
        if self.engine_commands is not None:
            for command in self.engine_commands:
                forced_states[command] = 0.
        if self.steering_commands is not None:
            for command in self.steering_commands:
                forced_states[command] = 0.
        # Overriding default values with values from x0
        forced_states.update(x0)
        # Applying forced states for statics computation
        forced_states.update({'dz_dt': 0.,
                             'psi': 0.,
                             'p': 0.,
                             'q': 0.,
                             'r': 0.,
                             'x': 0.,
                             'y': 0.})
        if self.mode is Mode.PPP:
            forced_states['dx_dt'] = self.ship_speed
        # Removing variables that must be solved for
        solving_for_flat = set() # solving_for may be a compound list so it must be flattened first
        for label in solving_for:
            if isinstance(label, list) or isinstance(label, set):
                solving_for_flat.update(label)
            else:
                solving_for_flat.add(label)
        for label in solving_for_flat.intersection(forced_states):
            del forced_states[label]
        return forced_states

    def _solve_problem(self,
                       model: xdyn.Xdyn_server_steady,
                       parameters: Parameters,
                       decoupled_solver_sequence: Iterable[Callable[[xdyn.Xdyn_server_steady, Parameters, Dict[str, float]], Dict[str, float]]],
                       coupled_solver: Callable[[xdyn.Xdyn_server_steady, Parameters, Dict[str, float]], Dict[str, float]],
                       x0: Dict[str, float] = None):
        if x0 is None:
            x = {}
        else:
            x = x0.copy()
        conv = False
        i = 0
        latest_failure = None
        while (not conv) and i<self.max_decoupled_iterations:
            i = i + 1
            local.logger.debug('Starting decoupled iteration %i', i)
            for solver in decoupled_solver_sequence:
                x.update(solver(model, parameters, x))

            try:
                x.update(coupled_solver(model, parameters, x))
                conv = True
                local.logger.debug('Coupled solving succeeded after decoupled iteration %i', i)
            except (ConvergenceError, OperationError) as error: # This is to catch cases when the multivariate root-finding algorithm diverges, which usually causes an OperationError
                latest_failure = error
                local.logger.debug('Coupled solving failed after decoupled iteration %i', i)
                continue
        if conv:
            return x
        else:
            raise ConvergenceError('Maximum number of decoupled iterations reached, and the coupled problem solving failed.', parameters) from latest_failure

    @log_stack(local, 'horizontal problem')
    def _solve_horizontal_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        return self._solve_problem(model, parameters, [self._solve_propulsion_problem, self._solve_leeway_problem, self._solve_course_keeping_problem], self._solve_coupled_horizontal_problem, x0)

    def _make_horizontal_problem(self, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = ['dy_dt']
        x_i = [x0['dy_dt'] if 'dy_dt' in x0 else 0.]
        axes = ['Fy']
        if self.mode is Mode.VPP:
            solving_for.append('dx_dt')
            x_i.append(x0['dx_dt'] if 'dx_dt' in x0 else 1.)
            axes.append('Fx')
        elif self.mode is Mode.PPP and self.engine_commands is not None:
            if self.engine_commands.intersection(x0):
                if x0[first(self.engine_commands)]!=0. and x0[first(self.engine_commands)]!=self.max_engine_command:
                    solving_for.append(self.engine_commands)
                    x_i.append(x0[first(self.engine_commands)])
                    axes.append('Fx')
                # (Else) If the engine command is saturated at 0. or max_engine_command, the propulsion DoF is ignored
            else:
                solving_for.append(self.engine_commands)
                x_i.append(1.)
                axes.append('Fx')
        if self.steering_commands is not None:
            solving_for.append(self.steering_commands)
            x_i.append(x0[first(self.steering_commands)] if self.steering_commands.intersection(x0) else 0.)
            axes.append('Mz')
        return solving_for, x_i, axes

    @log_stack(local, 'coupled problem')
    def _solve_coupled_horizontal_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        solving_for, x_i, axes = self._make_horizontal_problem(x0)

        forced_states = self._get_forced_states(x0, solving_for) # Forcing states that are not solved for

        res = self._solve_multivariate_root(model, x_i, solving_for, forced_states, axes)
        if res.success:
            return self._make_states(solving_for, res.x, forced_states)
        else:
            raise ConvergenceError('Failed to find a solution to the coupled problem: '+str(res), parameters, solving_for)

    @log_stack(local, 'propulsion problem')
    def _solve_propulsion_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        if self.mode is Mode.VPP:
            solving_for = 'dx_dt'
            x_i = x0['dx_dt'] if 'dx_dt' in x0 else 1.
            forced_states = self._get_forced_states(x0, [solving_for])
            axis = 'Fx'
            xmin = 0.
            xmax = self.max_speed
            Fx_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis], global_frame=True)[0]
            Fx_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis], global_frame=True)[0]
            if Fx_min<0.:
                self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
                raise ConvergenceError("At zero speed, the total force along X is negative. The ship is underpropelled and cannot reach a forward speed.", parameters, solving_for)
            elif Fx_max>0.:
                self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
                raise ConvergenceError("At maximum speed ("+str(xmax)+"m/s), the total force along X is positive. The ship is overpropelled, try increasing the maximum speed.", parameters, solving_for)
            else:
                return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax, global_frame=True)
        elif self.mode is Mode.PPP and self.engine_commands is not None:
            solving_for = self.engine_commands
            x_i = abs(x0[first(self.engine_commands)]) if self.engine_commands.intersection(x0) else 1.
            forced_states = self._get_forced_states(x0, [solving_for])
            axis = 'Fx'
            xmin = 0.
            xmax = self.max_engine_command
            Fx_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis], global_frame=True)[0]
            Fx_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis], global_frame=True)[0]
            if Fx_min>0.:
                self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
                local.logger.warning("For parameters %s at null engine command (%s=0.), the total force along X is positive. The ship is overpropelled for this speed. The simulation will continue but the equilibrium along the X axis will be ignored.", parameters, self.engine_commands)
                return {command: 0. for command in self.engine_commands}
            elif Fx_max<0.:
                self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
                local.logger.warning("For parameters %s at maximum engine command (%s=%f), the total force along X is negative. The ship is underpropelled, try increasing the maximum engine command or consider a more powerful propeller. The simulation will continue but the equilibrium along the X axis will be ignored.", parameters, self.engine_commands, xmax)
                return {command: self.max_engine_command for command in self.engine_commands}
            else:
                return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax, global_frame=True)
        else:
            return {}

    @log_stack(local, 'leeway problem')
    def _solve_leeway_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = 'dy_dt'
        x_i = x0['dy_dt'] if 'dy_dt' in x0 else 0.1
        forced_states = self._get_forced_states(x0, [solving_for])
        axis = 'Fy'
        xmax = self.max_speed
        xmin = -xmax
        Fy_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis], global_frame=True)[0]
        Fy_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis], global_frame=True)[0]
        if Fy_min<0. or Fy_max>0.:
            self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError("Side forces equilibrium cannot be reached. Maybe leeway resistance is not sufficient", parameters, solving_for)
        else:
            return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax, global_frame=True)

    @log_stack(local, 'course-keeping problem')
    def _solve_course_keeping_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        if self.steering_commands is not None:
            solving_for = self.steering_commands
            x_i = x0[first(self.steering_commands)] if self.steering_commands.intersection(x0) else 0.
            forced_states = self._get_forced_states(x0, [solving_for])
            axis = 'Mz'
            xmin = -float(self.max_steering_command)
            xmax = self.max_steering_command
            Mz_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis], global_frame=True)[0]
            Mz_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis], global_frame=True)[0]
            if Mz_min*Mz_max>0.:
                self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
                raise ConvergenceError("Course-keeping equilibrium cannot be reached. Maybe steering moment is not sufficient, or max steering command is too large", parameters, solving_for)
            else:
                return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax, global_frame=True)
        else:
            return {}

    @log_stack(local, 'hydrostatics')
    def _solve_hydrostatics(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        return self._solve_problem(model, parameters, [self._solve_sinkage_problem, self._solve_heel_problem, self._solve_trim_problem], self._solve_coupled_hydrostatics, x0)

    @log_stack(local, 'coupled problem')
    def _solve_coupled_hydrostatics(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = ['z', 'phi', 'theta']
        x_i = [x0['z'] if 'z' in x0 else 0.,
               x0['phi'] if 'phi' in x0 else 0.,
               x0['theta'] if 'theta' in x0 else 0.]
        axes = ['Fz', 'Mx', 'My']

        forced_states = self._get_forced_states(x0, solving_for) # Forcing states that are not solved for

        res = self._solve_multivariate_root(model, x_i, solving_for, forced_states, axes)
        if res.success:
            return dict(zip(solving_for, res.x))
        else:
            raise ConvergenceError('Failed to find a solution to the coupled problem: '+str(res), parameters, solving_for)

    @log_stack(local, 'sinkage problem')
    def _solve_sinkage_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = 'z'
        x_i = x0[solving_for] if solving_for in x0 else 0.
        forced_states = self._get_forced_states(x0, [solving_for])
        axis = 'Fz'
        xmax = self.max_sinkage
        xmin = -xmax
        Fz_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis], global_frame=True)[0]
        Fz_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis], global_frame=True)[0]
        if Fz_min*Fz_max>0.:
            self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError("Sinkage equilibrium cannot be reached. Maybe max sinkage allowed is too low", parameters, solving_for)
        else:
            return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax, global_frame=True)

    @log_stack(local, 'heel problem')
    def _solve_heel_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = 'phi'
        x_i = x0[solving_for] if solving_for in x0 else 0.
        forced_states = self._get_forced_states(x0, [solving_for])
        axis = 'Mx'
        xmax = self.max_heel
        xmin = -xmax
        Mx_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis])[0]
        Mx_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis])[0]
        if Mx_min*Mx_max>0.:
            self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError("Heel equilibrium cannot be reached. Maybe max heel allowed is too low, or the ship does not have enough lateral stability", parameters, solving_for)
        else:
            return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax)

    @log_stack(local, 'trim problem')
    def _solve_trim_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}
        solving_for = 'theta'
        x_i = x0[solving_for] if solving_for in x0 else 0.
        forced_states = self._get_forced_states(x0, [solving_for])
        axis = 'My'
        xmax = self.max_trim
        xmin = -xmax
        My_min = model.sum_of_forces(self._make_states([solving_for], [xmin], forced_states), [axis])[0]
        My_max = model.sum_of_forces(self._make_states([solving_for], [xmax], forced_states), [axis])[0]
        if My_min*My_max>0.:
            self.output_forces(model, solving_for, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError("Trim equilibrium cannot be reached. Maybe max trim allowed is too low, or the ship does not have enough longitudinal stability", parameters, solving_for)
        else:
            return self._solve_scalar_root(model, x_i, solving_for, forced_states, axis, parameters, xmin, xmax)

    @log_stack(local, 'global coupled problem')
    def _solve_coupled_problem(self, model: xdyn.Xdyn_server_steady, parameters: Parameters, x0: dict = None):
        if x0 is None:
            x0 = {}

        solving_for, x_i, axes = self._make_horizontal_problem(x0)

        x_i.extend([x0['z'] if 'z' in x0 else 0.,
                    x0['phi'] if 'phi' in x0 else 0.,
                    x0['theta'] if 'theta' in x0 else 0.])
        solving_for.extend(['z', 'phi', 'theta'])
        axes.extend(['Fz', 'Mx', 'My'])

        forced_states = self._get_forced_states(x0, solving_for) # Forcing states that are not solved for

        res = self._solve_multivariate_root(model, x_i, solving_for, forced_states, axes)
        if res.success:
            states = self._make_states(solving_for, res.x)
            local.logger.debug('Found multivariate root %s.', states)
            return states
        else:
            raise ConvergenceError('Failed to find a solution to the coupled problem: '+str(res), parameters, solving_for)

    def _solve_multivariate_root(self, model, x0, labels, forced_states, axes):
        def f_root(x):
            return model.sum_of_forces(self._make_states(labels, x, forced_states), axes)
        options = {'xtol': self.tolerance}
        if self.coupled_algorithm in ['hybr', 'df-sane']:
            options['maxfev'] = self.maxiter
        else:
            options['maxiter'] = self.maxiter
        if self.coupled_algorithm in ['linearmixing','diagbroyden','excitingmixing','broyden1','broyden2','anderson','krylov','df-sane']:
            options['fatol'] = self.tolerance
        if self.coupled_algorithm == 'df-sane':
            options['fnorm'] = max
        return root(f_root, x0, method=self.coupled_algorithm, options=options, tol=self.tolerance)

    def _solve_scalar_root(self, model: xdyn.Xdyn_server_steady, x0: float, label: Union[str, Set[str]], forced_states: Dict[str, float], axis: str, parameters: Parameters, xmin: float, xmax: float, global_frame: bool = False):
        if self.always_save_forces_debug_file:
            self.output_forces(model, label, axis, xmin, xmax, forced_states, parameters)
        def f_root(x):
            return model.sum_of_forces(self._make_states([label], [x], forced_states), [axis], global_frame)[0]
        try:
            res = root_scalar(f_root, x0=x0, method=self.scalar_algorithm, bracket=(xmin, xmax), rtol=self.tolerance)
        except Exception as error:
            # self.plot_curve(f_root, xmin, xmax, label, axis)
            self.output_forces(model, label, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError('Failed to solve for the variable '+str(label)+' with the following being fixed: '+str(forced_states), parameters, label) from error
        if not res.converged:
            self.output_forces(model, label, axis, xmin, xmax, forced_states, parameters)
            raise ConvergenceError('Failed to solve for the variable '+str(label)+': \n'+str(res)+'\n where x is '+str(label)+' and the following is fixed: '+str(forced_states), parameters, label)
        else:
            local.logger.debug('Found scalar root %s=%f.', label, res.root)
            return self._make_state(label, res.root)

    def _make_states(self, labels, x0, forced_states=None):
        if forced_states is None:
            forced_states = {}
        states = {}
        for i, label in enumerate(labels):
            states.update(self._make_state(label, x0[i]))
        states.update(forced_states)
        return states

    def _make_state(self, label: Union[str, Set[str]], value: float):
        state = {}
        if isinstance(label, list) or isinstance(label, set): # Duplicated commands (multiple labels for one value)
            for duplicate in label:
                state[duplicate] = value
        else:
            state[label] = value
        return state

    # def plot_curve(self, fun, x_min, x_max, x_label='x', y_label='F'):
    #     f = np.vectorize(fun)
    #     x = np.linspace(x_min, x_max, 100)
    #     fig, ax = plt.subplots()
    #     ax.plot(x, f(x))
    #     ax.set_xlabel(x_label)
    #     ax.set_ylabel(y_label)
    #     ax.grid()

    def output_forces(self, model, label, axis, xmin, xmax, forced_states, parameters: Parameters):
        """Writes a CSV file with all the forces between xmin and xmax.
        """
        if isinstance(label, str):
            formatted_label = label
        elif isinstance(label, list) or isinstance(label, set):
            formatted_label = ','.join(label)
        else:
            formatted_label = str(label)
        filename = 'debug_'+axis+'('+formatted_label+')_'+parameters.as_formatted_string()+'.csv'
        with open(filename, 'w', encoding='UTF-8') as file:
            writer = csv.writer(file, 'excel')
            states = forced_states.copy()
            states.update(self._make_state(label, xmin))
            results = model.get_system_states(states)
            labels = [formatted_label]
            row = [xmin]
            for force_label, value in results.forces.items():
                if force_label.frame == 'NED' and force_label.axis == axis and force_label.name != 'fictitious forces':
                    labels.append(force_label.as_xdyn())
                    row.append(value)
            writer.writerow(labels)
            writer.writerow(row)
            x = np.linspace(xmin, xmax, 100)
            for xi in x[1:]:
                states.update(self._make_state(label, xi))
                results = model.get_system_states(states)
                row = [xi]
                for force_label in labels[1:]:
                    row.append(results.forces[ForceLabel.from_xdyn(force_label)])
                writer.writerow(row)
            local.logger.info("Outputted debug file %s", filename)

    def check_equilibrium(self, states, model, axes=None):
        sum_of_forces = model.sum_of_forces(list(states.values()), list(states.keys()), axes=axes)
        return max(sum_of_forces)<self.tolerance


# Class used to manage steady case computations and results
class Statics:
    """This class manages static computations.
    """
    def __init__(self, xdyn_model_file, statics_input, mode: Mode, engine_command, steering_commands, ship_speed, wind_model):
        try:
            self.steady_state_computer = DecoupledRootFindingStaticsComputer(statics_input, xdyn_model_file, mode, engine_command, steering_commands, ship_speed, wind_model)
            self.post_processor = StaticResultsPostProcessor(statics_input, xdyn_model_file, mode, engine_command, steering_commands)
        except KeyError as error:
            raise MissingParameter(str(error), 'statics') from error

        self.x0_initial = get_initial_attitude(self.steady_state_computer.yaml_input)
        self.initial_hydrostatics = None
        try:
            if 'solve initial hydrostatics' in statics_input:
                if bool(statics_input['solve initial hydrostatics']):
                    self.initial_hydrostatics = self.steady_state_computer.compute_statics(x0 = self.x0_initial)
            elif self.steady_state_computer.solve_hydrostatics:
                local.logger.info("Solving initial hydrostatics by default.")
                self.initial_hydrostatics = self.steady_state_computer.compute_statics(x0 = self.x0_initial)
        except Exception as error:
            local.logger.error('Error occurred during initial hydrostatics computation: %s. Skipping initial hydrostatics computation.', error)

    def compute(self, parameters: Parameters):
        local.logger = ParametersLoggerAdapter(logging.getLogger('xWASP_CN'), parameters.dict())
        if self.initial_hydrostatics is not None:
            res = self.steady_state_computer.compute_statics(parameters, x0 = self.initial_hydrostatics.system_states.states)
        else:
            res = self.steady_state_computer.compute_statics(parameters, x0 = self.x0_initial)
        res.computed_data = self.post_processor.post_process(res, self.initial_hydrostatics.system_states.states if self.initial_hydrostatics is not None else self.x0_initial)
        return res

    def output_initial_hydrostatics(self, outputter):
        if self.initial_hydrostatics is not None:
            outputter.write_hydrostatics_result(self.initial_hydrostatics)


class StaticResultsPostProcessor:
    def __init__(self, statics_config, xdyn_model_file, mode, engine_commands: Set[str], steering_commands: Set[str]):
        xdyn_model_input = get_yaml(xdyn_model_file)
        self.ship_name = str(xdyn_model_input['bodies'][0]['name'])
        self.sail_names = None
        if 'sail models' in statics_config:
            self.sail_names = list(statics_config['sail models'])
            check_yaml_for_forces(xdyn_model_input, self.sail_names, 'statics: sail models')
        else:
            local.logger.info("Sail model names not provided: the sail power coverage will not be computed.")
        self.propeller_names = None
        if 'propeller models' in statics_config:
            self.propeller_names = list(statics_config['propeller models'])
            check_yaml_for_forces(xdyn_model_input, self.propeller_names, 'statics: propeller models')

        self.angle_unit = 'rad'
        if statics_config['output angle unit']:
            self.angle_unit = parse_angle_unit(statics_config['output angle unit'])

        self.mode = mode
        self.engine_commands = engine_commands
        self.steering_commands = steering_commands

    def post_process(self, static_results: StaticResults, initial_hydrostatic_states: Dict[str, float] = None):
        static_states = static_results.system_states
        results = {}
        if self.sail_names is not None:
            results['sail power coverage'] = self.sail_power_coverage(static_states.forces)
        results['heeling angle'] = self.heeling_angle(static_states.states, initial_hydrostatic_states)
        results['trim angle'] = self.trim_angle(static_states.states, initial_hydrostatic_states)
        results['sinkage'] = self.sinkage(static_states.states, initial_hydrostatic_states)
        results['leeway angle'] = self.leeway_angle(static_states.states)
        if self.mode is Mode.PPP:
            if self.engine_commands is not None:
                results['engine command'] = UnitValue(static_states.commands[first(self.engine_commands)], '-')
            else:
                results['required propulsion power'] = UnitValue(static_states['u']*(-static_states.forces[ForceLabel('Fx', 'sum of forces', self.ship_name, self.ship_name)]), 'W')
        if self.mode is Mode.VPP:
            results['forward speed'] = UnitValue(static_states.states['dx_dt'], 'm/s')
        results['speed over ground'] = UnitValue(sqrt(static_states.states['dx_dt']**2 + static_states.states['dy_dt']**2), 'm/s')
        results['leeway speed'] = UnitValue(static_states.states['dy_dt'], 'm/s')
        if self.steering_commands is not None:
            results['steering command'] = UnitValue(static_states.commands[first(self.steering_commands)], '-')
        else:
            results['required steering moment'] = UnitValue(-static_states.forces[ForceLabel('Mz', 'sum of forces', self.ship_name, 'NED')], 'N.m')
        return results

    def sail_power_coverage(self, forces: Dict[ForceLabel, str]):
        mean_sail_force = 0.
        for sail in self.sail_names:
            mean_sail_force += forces[ForceLabel('Fx',sail,self.ship_name,self.ship_name)]
        mean_prop_force = 0.
        if self.propeller_names is not None:
            for prop in self.propeller_names:
                mean_prop_force += forces[ForceLabel('Fx',prop,self.ship_name,self.ship_name)]
        mean_prop_force += forces[ForceLabel('Fx','blocked states',self.ship_name,self.ship_name)]
        mean_prop_force -= forces[ForceLabel('Fx','sum of forces',self.ship_name,self.ship_name)]
        sail_coverage = mean_sail_force/(mean_prop_force+mean_sail_force)
        return UnitValue(sail_coverage, '-')

    def heeling_angle(self, states: Dict[str, float], hydrostatic_states: Dict[str, float] = None):
        heeling = states['phi']
        if hydrostatic_states is not None:
            heeling -= hydrostatic_states['phi']
        if self.angle_unit=='deg':
            return UnitValue(np.degrees(heeling), 'deg')
        else:
            return UnitValue(heeling, 'rad')

    def trim_angle(self, states: Dict[str, float], hydrostatic_states: Dict[str, float] = None):
        trim = states['theta']
        if hydrostatic_states is not None:
            trim -= hydrostatic_states['theta']
        if self.angle_unit=='deg':
            return UnitValue(np.degrees(trim), 'deg')
        else:
            return UnitValue(trim, 'rad')

    def sinkage(self, states: Dict[str, float], hydrostatic_states: Dict[str, float] = None):
        sinkage = states['z']
        if hydrostatic_states is not None:
            sinkage -= hydrostatic_states['z']
        return UnitValue(sinkage, 'm')

    def leeway_angle(self, states: Dict[str, float]):
        leeway = atan2(states['dy_dt'],states['dx_dt'])
        if self.angle_unit=='deg':
            return UnitValue(np.degrees(leeway), 'deg')
        else:
            return UnitValue(leeway, 'rad')

    # def get_radial_results(self,ship_speed,wind_speed,address=[]):
    #     if not ship_speed in self.ship_speeds:
    #         raise InputError('ship_speed',ship_speed,'The requested ship speed is not computed by this test instance')
    #     elif not wind_speed in self.wind_speeds:
    #         raise InputError('wind_speed',wind_speed,'The requested wind speed is not computed by this test instance')
    #     res=[]
    #     for wd in self.wind_directions:
    #         res.append(deepcopy(recursive_access_in_dict(self.results[Parameters(ship_speed,wind_speed,wd)],address)))
    #     return res

    # def plot_standard_polar_results(self,title,radial_label,address=[],factor=1.):
    #     wd_radians=np.radians(self.wind_directions)
    #     figs=[]
    #     for ss in self.ship_speeds:
    #         fig=plt.figure()
    #         ax=fig.add_subplot(projection='polar',label=str(ss))
    #         ax.set_theta_direction(-1)
    #         if max(wd_radians)>np.pi:
    #             ax.set_xticks(np.radians(range(0,360,30)))
    #             ax.set_thetalim(0,2*np.pi)
    #         else:
    #             ax.set_xticks(np.radians(range(0,181,30)))
    #             ax.set_thetalim(0,np.pi)
    #         ax.set_theta_zero_location("N")
    #         for ws in self.wind_speeds:
    #             data = float(factor)*np.array(self.get_radial_results(ss,ws,address))
    #             ax.plot(wd_radians,data,label=str(ws)+' '+self.units['wind speed'],marker='o')
    #         ax.legend(title='Wind speed')
    #         ax.set_ylabel(str(radial_label))
    #         ax.set_title(str(title)+' for Vs = '+str(ss)+self.units['ship speed'])
    #         ax.autoscale(axis='y', tight=True)
    #         figs.append(fig)
    #     return figs

    # def plot_results(self):
    #     self.plot_standard_polar_results('Sails power coverage','%',address=['sail power coverage','value'],factor=100.)
    #     self.plot_standard_polar_results('Ship heeling angle',self.angle_unit,address=['heeling angle','value'])
    #     self.plot_standard_polar_results('Ship drift angle',self.angle_unit,address=['drift angle','value'])
    #     if self.mode is Mode.PPP:
    #         if self.engine_command is not None:
    #             self.plot_standard_polar_results('Engine command','-',address=['engine command','value'])
    #         else:
    #             self.plot_standard_polar_results('Required propulsion power','kW',address=['required propulsion power','value'],factor=1e-3)
    #     if self.mode is Mode.VPP:
    #         self.plot_standard_polar_results('Ship forward speed','m/s',address=['forward speed','value'])
    #     if self.steering_commands is not None:
    #         self.plot_standard_polar_results('Steering command','-',address=['steering command','value'])
    #     else:
    #         self.plot_standard_polar_results('Required steering moment','kN.m',address=['required steering moment','value'],factor=1e-3)
    #     for var in self.extra_variables:
    #         self.plot_standard_polar_results(var,'[SI]',address=[var],factor=1.)
