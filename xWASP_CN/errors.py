### EXCEPTIONS DEFINITIONS ###
class Error(Exception):
    """Base class for exceptions in this module.
    """
    pass

class InvalidInputError(Error):
    """Exception raised for errors in the input.

    Attributes
    ----------
    variable
        name of the invalid input variable
    expression
        input expression in which the error occurred
    message : str
        explanation of the error
    """

    def __init__(self, variable, expression, message):
        self.variable = variable
        self.expression = expression
        self.message = message

    def __str__(self):
        return str(self.message)+": parameter '"+str(self.variable)+"' has invalid value '"+str(self.expression)+"'"

class MissingParameter(Error):
    """Exception raised for missing parameters in the input.

    Attributes
    ----------
    parameter : str
        Name of the missing parameter (or section if section=None).
    section : str
        Section of the input in which the parameter should be located.
    """
    def __init__(self, parameter: str, section: str = None):
        self.parameter = str(parameter)
        self.section = str(section)

    def __str__(self):
        if self.section is not None:
            return "Missing parameter '"+self.parameter+"' in section '"+self.section+"'."
        else:
            return "Missing section '"+self.parameter+"' from input."

class ConvergenceError(Error):
    """Exception raised when a simulation fails to converge.

    Attributes
    ----------
    message : str
        explanation of the error
    parameters : dict
        parameters of the computation that did not converge
    """

    def __init__(self, message, parameters, not_converged_variables=None):
        self.parameters = parameters
        self.message = message
        self.not_converged_variables = not_converged_variables

    def __str__(self):
        message = "A computation failed to converge: "+str(self.message)+". The parameters of the computations were: "+str(self.parameters)
        if self.not_converged_variables is not None:
            message = message + ". The variables that did not converge were: "+str(self.not_converged_variables)
        return message

class ExecutionError(Error):
    """Exception raised when an error occurs during the execution of a computation.

    Attributes
    ----------
    message : str
        explanation of the error
    parameters : dict
        parameters of the computation that failed
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "An error occurred during the execution: "+str(self.message)

class OperationError(Error):
    """Exception raised for errors occurring while trying to do actions that have unmet pre-requisites, for example actions that require the server to be launched.

    Attributes
    ----------
    message : str
        explanation of the error
    """

    def __init__(self, message):
        self.message=message

    def __str__(self):
        return str(self.message)

class InvalidValueError(Error):
    """Exception raised when a variable has an invalid value (e.g. a float is NaN or Inf).

    Attributes
    ----------
    variable: str
        Variable that is invalid
    message : str
        Explanation of the error
    """

    def __init__(self, variable, message):
        self.variable = str(variable)
        self.message = str(message)

    def __str__(self):
        return 'NaN detected in: '+self.variable+'. '+self.message
