from dataclasses import dataclass
from enum import Flag, auto
from typing import Dict, List
import re
import logging
from threading import local

from xWASP_CN.errors import InvalidInputError

local = local()
local.logger = logging.getLogger('xWASP_CN')

# Helper functions
functRE = re.compile(r"(?P<function>[a-zA-Z_]\w*)\((?P<arguments>[a-zA-Z_][a-zA-Z0-9_& -]*(?:,[a-zA-Z_][a-zA-Z0-9_& -]*)*)\)")
forceRE = re.compile(r"(?P<axis>[FM][xyz])\((?P<name>[a-zA-Z_][a-zA-Z0-9_& -]*(\([a-zA-Z_][a-zA-Z0-9_& -]*\))?),(?P<body>[a-zA-Z_][a-zA-Z0-9_& -]*),(?P<frame>[a-zA-Z_][a-zA-Z0-9_& -]*)\)")
sigRE = re.compile(r"(?P<target>[a-zA-Z_]\w*)\((?P<variable>[a-zA-Z_][a-zA-Z0-9_& -]*)\)")

class AutoNameFlag(Flag):
    @staticmethod
    def _generate_next_value_(name, start, count, last_values):
        return name

class Mode(AutoNameFlag):
    """This enumeration defines a mode for xWASP: VPP for Velocity Prediction and PPP for Power Prediction.
       The HS (hydrostatics) mode is used internally by StaticsComputer.
    """
    VPP = auto()
    PPP = auto()
    HS = auto()

@dataclass
class UnitValue:
    value: float
    unit: str

    @classmethod
    def from_dict(cls, dict_input: Dict[str, float]):
        try:
            return cls(float(dict_input['value']), str(dict_input['unit']))
        except KeyError as error:
            raise InvalidInputError('dict_input', dict_input, "Missing field '"+str(error)+"'.") from error
        except ValueError as error:
            raise InvalidInputError('dict_input', dict_input, "str(error)") from error

@dataclass
class UnitValues:
    value: List[float]
    unit: str

    @classmethod
    def from_dict(cls, dict_input: Dict[str, List[float]]):
        try:
            return cls(dict_input['values'], str(dict_input['unit']))
        except KeyError as error:
            raise InvalidInputError('dict_input', dict_input, "Missing field '"+str(error)+"'.") from error
        except ValueError as error:
            raise InvalidInputError('dict_input', dict_input, "str(error)") from error

@dataclass(frozen=True)
class ParametersUnits:
    wind_speed: str = 'm/s'
    wind_angle: str = 'deg'

    @classmethod
    def from_dict(cls, dict_input: Dict[str, str]):
        """Parses the dict input for units into a ParametersUnits.
        """
        if 'wind speed' in dict_input:
            wind_speed_unit = dict_input['wind speed']
        else:
            local.logger.info("Defaulting wind speed unit to 'm/s'")
            wind_speed_unit = 'm/s'
        if 'wind angle' in dict_input:
            wind_angle_unit = dict_input['wind angle']
        else:
            local.logger.info("Defaulting wind angle unit to 'deg'")
            wind_angle_unit = 'deg'
        return cls(wind_speed_unit, wind_angle_unit)


@dataclass(frozen=True)
class Parameters:
    """This class stores a set of computation parameters.
    """
    wind_speed: float # True Wind Speed
    wind_angle: float # True Wind Angle
    units: ParametersUnits = ParametersUnits('m/s','deg')

    def __str__(self):
        return str(self.dict())

    def as_formatted_string(self):
        """Returns a string suitable for inclusion in a file name.
        """
        return 'TWS_'+str(self.wind_speed)+'_TWA_'+str(self.wind_angle)

    def dict(self):
        return {'TWS':self.wind_speed,'TWA':self.wind_angle}

@dataclass(frozen = True)
class ForceLabel:
    """This class represents a force label.
    """
    axis: str # Axis, among Fx, Fy, Fz, Mx, My and Mz
    name: str # Name of the force
    body: str # Name of the body on which the force is applied
    frame: str # Name of the frame in which the force is expressed

    def as_xdyn(self):
        """The force label as expressed in xdyn, e.g. 'Fz(gravity,ship,NED)'.
        """
        return self.axis + "(" + self.name + "," + self.body + "," + self.frame + ")"

    @classmethod
    def from_xdyn(cls, xdyn_force_label):
        """Parses a force label as expressed in xdyn into a ForceLabel.
        """
        match = forceRE.match(xdyn_force_label)
        if match:
            return cls(match.group('axis'), match.group('name'), match.group('body'), match.group('frame'))
        else:
            raise InvalidInputError("xdyn_force_label", xdyn_force_label, "The string does not have a force label format, e.g. 'Fz(gravity,ship,NED)'.")

    def is_force(self):
        return self.axis[0]=='F'

    def is_moment(self):
        return self.axis[0]=='M'

@dataclass
class SystemState:
    """This class represents the complete state of a system simulated in xdyn at a given time.
    """
    states: Dict[str, float] # Body state variables
    forces: Dict[ForceLabel, float] # Static state forces
    commands: Dict[str, float] = None # Static state commands
    other: Dict[str, float] = None # Other data which could be computed by xdyn, usually empty

    def __getitem__(self, key):
        force_match = forceRE.match(key)
        if force_match:
            label = ForceLabel(force_match.group('axis'), force_match.group('name'), force_match.group('body'), force_match.group('frame'))
            if label in self.forces:
                return self.forces[label]
        elif key in {'u','v','w','p','q','r','x','y','z','phi','theta','psi'}:
            return self.states[key]
        elif self.commands is not None and key in self.commands:
            return self.commands[key]
        elif self.other is not None and key in self.other:
            return self.other[key]
        else:
            raise InvalidInputError("key", key, "Variable not found in system states.")

@dataclass
class SystemStateHistory:
    """This class represents the complete state history of a system simulated in xdyn.
    """
    time: List[float]
    states: Dict[str, List[float]]
    forces: Dict[ForceLabel, List[float]]
    commands: Dict[str, List[float]] = None
    other: Dict[str, List[float]] = None

    def __getitem__(self, key):
        force_match = forceRE.match(key)
        if force_match:
            label = ForceLabel(force_match.group('axis'), force_match.group('name'), force_match.group('body'), force_match.group('frame'))
            if label in self.forces:
                return self.forces[label]
        elif key in {'u','v','w','p','q','r','x','y','z','phi','theta','psi'}:
            return self.states[key]
        elif key in self.commands:
            return self.commands[key]
        elif key in self.other:
            return self.other[key]
        else:
            raise InvalidInputError("key", key, "Variable not found in system states.")


@dataclass
class StaticResults:
    """This class stores results from a static computation.
    """
    system_states: SystemState # Static system states
    computed_data: Dict[str, UnitValue] = None
    convergence_data: dict = None # Convergence data, depending on algorithm

@dataclass
class DynamicResults:
    """This class stores results from a dynamic computation.
    """
    test_name: str
    test_type: str
    computed_data: Dict[str, UnitValue]
    time_series: SystemStateHistory = None
