import itertools
import time

from typing import Dict, List
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from cycler import cycler

from cosimulation_pb2 import CosimulationStatesEuler

from xWASP_CN.errors import InvalidInputError
from xWASP_CN.data_structures import ForceLabel

# Class used to plot the results of Xdyn_server, live or not
class ResultsPlot:
    def __init__(self, states: CosimulationStatesEuler, ship_name: str = None, extra_observations: Dict[str, List[float]]=None):
        self._position_fig, self._position_axes = plt.subplots(2, 3, sharex=True)
        self._position_fig.suptitle('Position of body in NED frame')
        self._position_plots = {}
        self._position_labels = {(0, 0): 'x', (0, 1): 'y', (0, 2): 'z', (1, 0): 'phi', (1, 1): 'theta', (1, 2): 'psi'}
        self._position_plots['x'] = self._position_axes[0, 0].add_line(Line2D(states.t, states.x, ls='-', c='k'))
        self._position_axes[0, 0].set_title('x')
        self._position_axes[0, 0].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[0, 0].grid()
        self._position_plots['y'] = self._position_axes[0, 1].add_line(Line2D(states.t, states.y, ls='-', c='k'))
        self._position_axes[0, 1].set_title('y')
        self._position_axes[0, 1].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[0, 1].grid()
        self._position_plots['z'] = self._position_axes[0, 2].add_line(Line2D(states.t, states.z, ls='-', c='k'))
        self._position_axes[0, 2].set_title('z')
        self._position_axes[0, 2].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[0, 2].grid()
        self._position_plots['phi'] = self._position_axes[1, 0].add_line(Line2D(states.t, states.phi, ls='-', c='k'))
        self._position_axes[1, 0].set_title('phi')
        self._position_axes[1, 0].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[1, 0].grid()
        self._position_plots['theta'] = self._position_axes[1, 1].add_line(Line2D(states.t, states.theta, ls='-', c='k'))
        self._position_axes[1, 1].set_title('theta')
        self._position_axes[1, 1].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[1, 1].grid()
        self._position_plots['psi'] = self._position_axes[1, 2].add_line(Line2D(states.t, states.psi, ls='-', c='k'))
        self._position_axes[1, 2].set_title('psi')
        self._position_axes[1, 2].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._position_axes[1, 2].grid()

        self._velocity_fig, self._velocity_axes = plt.subplots(2, 3, sharex=True)
        self._velocity_fig.suptitle('Velocity of body relative to NED frame in body frame')
        self._velocity_plots = {}
        self._velocity_labels = {(0, 0): 'u', (0, 1): 'v', (0, 2): 'w', (1, 0): 'p', (1, 1): 'q', (1, 2): 'r'}
        self._velocity_plots['u'] = self._velocity_axes[0, 0].add_line(Line2D(states.t, states.u, ls='-', c='k'))
        self._velocity_axes[0, 0].set_title('u')
        self._velocity_axes[0, 0].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[0, 0].grid()
        self._velocity_plots['v'] = self._velocity_axes[0, 1].add_line(Line2D(states.t, states.v, ls='-', c='k'))
        self._velocity_axes[0, 1].set_title('v')
        self._velocity_axes[0, 1].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[0, 1].grid()
        self._velocity_plots['w'] = self._velocity_axes[0, 2].add_line(Line2D(states.t, states.w, ls='-', c='k'))
        self._velocity_axes[0, 2].set_title('w')
        self._velocity_axes[0, 2].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[0, 2].grid()
        self._velocity_plots['p'] = self._velocity_axes[1, 0].add_line(Line2D(states.t, states.p, ls='-', c='k'))
        self._velocity_axes[1, 0].set_title('p')
        self._velocity_axes[1, 0].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[1, 0].grid()
        self._velocity_plots['q'] = self._velocity_axes[1, 1].add_line(Line2D(states.t, states.q, ls='-', c='k'))
        self._velocity_axes[1, 1].set_title('q')
        self._velocity_axes[1, 1].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[1, 1].grid()
        self._velocity_plots['r'] = self._velocity_axes[1, 2].add_line(Line2D(states.t, states.r, ls='-', c='k'))
        self._velocity_axes[1, 2].set_title('r')
        self._velocity_axes[1, 2].ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._velocity_axes[1, 2].grid()

        self._traj_fig, self._traj_axe = plt.subplots()
        self._traj_fig.suptitle('Horizontal trajectory of body in NED frame')
        self._traj_plot = self._traj_axe.add_line(Line2D(states.x, states.y))
        self._traj_axe.ticklabel_format(axis='y', style='sci', useOffset=True, scilimits=(0, 0))
        self._traj_axe.grid()
        self._traj_axe.set_aspect('equal')

        self.ship_name = ship_name

        self._force_fig = {}
        self._force_axes = {}
        if self.ship_name is not None:
            self._force_fig[self.ship_name], self._force_axes[self.ship_name] = plt.subplots(2, 3, sharex=True)
            self._force_fig[self.ship_name].suptitle('Forces in body frame')
        self._force_fig['NED'], self._force_axes['NED'] = plt.subplots(2, 3, sharex=True)
        self._force_fig['NED'].suptitle('Forces in NED frame')
        self._force_labels = {(0, 0): 'Fx', (0, 1): 'Fy', (0, 2): 'Fz', (1, 0): 'Mx', (1, 1): 'My', (1, 2): 'Mz'}
        self._force_index = {'Fx': (0, 0), 'Fy': (0, 1), 'Fz': (0, 2), 'Mx': (1, 0), 'My': (1, 1), 'Mz': (1, 2)}
        for i, j in itertools.product(range(2), range(3)):
            if self.ship_name is not None:
                self._force_axes[self.ship_name][i, j].set_title(
                    self._force_labels[i, j])
                self._force_axes[self.ship_name][i, j].ticklabel_format(
                    axis='y', style='sci', useOffset=True, scilimits=(0, 0))
                self._force_axes[self.ship_name][i, j].grid()
                self._force_axes[self.ship_name][i, j].set_prop_cycle(
                    color=['red', 'green', 'blue'])
            self._force_axes['NED'][i, j].set_title(self._force_labels[i, j])
            self._force_axes['NED'][i, j].ticklabel_format(
                axis='y', style='sci', useOffset=True, scilimits=(0, 0))
            self._force_axes['NED'][i, j].grid()
            self._force_axes['NED'][i, j].set_prop_cycle(
                color=['red', 'green', 'blue'])
        self._force_plots = {}
        color_cycle = cycler(color=['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd',
                                        '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'])()
        attributed_colors = {}
        if extra_observations is not None:
            for force, force_data in extra_observations.items():
                try:
                    label = ForceLabel.from_xdyn(force)
                except InvalidInputError:
                    continue
                if label.name in attributed_colors:
                    color = attributed_colors[label.name]
                else:
                    color = next(color_cycle)
                    attributed_colors[label.name] = color
                axe_index = self._force_index[label.axis]
                if label.frame == 'NED':
                    self._force_plots[label] = self._force_axes['NED'][axe_index].add_line(
                        Line2D(states.t, force_data, label=label.name, **color))
                if label.frame == self.ship_name:
                    self._force_plots[label] = self._force_axes[self.ship_name][axe_index].add_line(
                        Line2D(states.t, force_data, label=label.name, **color))

        self._force_axes[self.ship_name][0, 2].legend()
        self._force_axes['NED'][0, 2].legend()

        self._extra_figs = {}
        self._extra_axes = {}
        self._extra_plots = {}
        plt.show()

    def update(self, states: CosimulationStatesEuler, extra_observations: Dict[str, List[float]]=None):
        self._position_plots['x'].set_data(states.t, states.x)
        self._position_plots['y'].set_data(states.t, states.y)
        self._position_plots['z'].set_data(states.t, states.z)
        self._position_plots['phi'].set_data(states.t, states.phi)
        self._position_plots['theta'].set_data(states.t, states.theta)
        self._position_plots['psi'].set_data(states.t, states.psi)
        self._velocity_plots['u'].set_data(states.t, states.u)
        self._velocity_plots['v'].set_data(states.t, states.v)
        self._velocity_plots['w'].set_data(states.t, states.w)
        self._velocity_plots['p'].set_data(states.t, states.p)
        self._velocity_plots['q'].set_data(states.t, states.q)
        self._velocity_plots['r'].set_data(states.t, states.r)
        self._traj_plot.set_data(states.x, states.y)
        for i, j in itertools.product(range(2), range(3)):
            self._position_axes[i, j].relim()
            self._position_axes[i, j].autoscale_view()
            self._velocity_axes[i, j].relim()
            self._velocity_axes[i, j].autoscale_view()
        self._traj_axe.relim()
        self._traj_axe.autoscale_view()
        self._position_fig.canvas.draw()
        self._velocity_fig.canvas.draw()
        self._traj_fig.canvas.draw()

        if extra_observations is not None:
            for force, force_data in extra_observations.items():
                try:
                    label = ForceLabel.from_xdyn(force)
                except InvalidInputError:
                    continue
                if label in self._force_plots:
                    self._force_plots[label].set_data(states.t, force_data)
            for i, j in itertools.product(range(2), range(3)):
                if self.ship_name is not None:
                    self._force_axes[self.ship_name][i, j].relim()
                    self._force_axes[self.ship_name][i, j].autoscale_view()
                self._force_axes['NED'][i, j].relim()
                self._force_axes['NED'][i, j].autoscale_view()
            if self.ship_name is not None:
                self._force_fig[self.ship_name].canvas.draw()
            self._force_fig['NED'].canvas.draw()
        time.sleep(1e-6)

    def custom_plot(self, x_label, x_values, y_labels, y_values):
        if len(y_labels) != len(y_values):
            raise InvalidInputError('y_labels', y_labels, 'There must be as many labels as there are data vectors.')
        fig, axes = plt.subplots()
        for i, y_label in enumerate(y_labels):
            plot = axes.plot(x_values, y_values[i], label=y_label)
            labels = (x_label, y_label)
            self._extra_figs[labels] = fig
            self._extra_axes[labels] = axes
            self._extra_plots[labels] = plot[0]
        axes.set_xlabel(x_label)
        axes.relim()
        axes.autoscale_view()
        axes.legend()
        axes.grid()

    def update_custom_plots(self, history_getter):
        for labels, plot in self._extra_plots.items():
            plot.set_xdata(history_getter(labels[0]))
            plot.set_ydata(history_getter(labels[1]))
            self._extra_axes[labels].relim()
            self._extra_axes[labels].autoscale_view()
            self._extra_figs[labels].canvas.draw()
        time.sleep(1e-6)

    def __del__(self):
        plt.close(self._position_fig)
        plt.close(self._velocity_fig)
        plt.close(self._traj_fig)
        for fig in self._extra_figs.values():
            plt.close(fig)
        #plt.close(self._forceFig[self.shipName])
        #plt.close(self._forceFig['NED'])
