#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
import random
from pathlib import Path
from typing import Set
import logging
from threading import local

import numpy as np
import yaml
#import matplotlib.pyplot as plt

import xWASP_CN.xdyn_gRPC as xdyn
from xWASP_CN.errors import InvalidInputError,ConvergenceError, MissingParameter, OperationError
from xWASP_CN.yaml_helpers import add_simple_heading_controller, get_yaml, get_modified_yaml, valid_UV, parse_angle_unit, add_constant_propulsion_force, get_as_radians, first
from xWASP_CN.data_structures import Mode, Parameters, ForceLabel, SystemState, DynamicResults, UnitValue
from xWASP_CN.loggers import ParametersLoggerAdapter

np.seterr(invalid='raise')

local = local()
local.logger = logging.getLogger('xWASP_CN')

random.seed()

def numpy_float64_representer(dumper, value):
    return dumper.represent_scalar('tag:yaml.org,2002:float', repr(float(value)).lower())
yaml.add_representer(np.float64, numpy_float64_representer)

# Base class definition
class DynamicComputer(ABC):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__()
        self.input = test
        self.config = config
        self.mode = mode
        self.engine_commands = engine_commands
        self.steering_commands = steering_commands
        self.wind_model = wind_model

        self.test_name = str(test['name'])
        self.test_type = str(test['type'])
        self.yaml = get_yaml(input_file)
        self.input_path = Path(input_file).resolve().parent
        self.ship_name = str(self.yaml['bodies'][0]['name'])

        if self.engine_commands is None and self.mode==Mode.PPP:
            local.logger.warning("In the absence of a propeller model (and matching engine command) in PPP mode, a constant propulsive force will be applied in the '%s' dynamic test.", self.test_name)

        self.heading_controller = None
        if self.steering_commands is None:
            if 'heading controller' in config:
                self.heading_controller = {}
                self.heading_controller['T'] = float(config['heading controller']['T'])
                self.heading_controller['ksi'] = float(config['heading controller']['ksi'])
                self.steering_commands = {'heading_controller(psi_co)'}
                local.logger.warning("In the absence of a steering model (and matching steering command), a simple heading controller force will be applied in the '%s' dynamic test.", self.test_name)
            else:
                raise OperationError("A steering model (and matching steering command) OR heading controller parameters are required for dynamic computations.")

        self.save_time_series = False
        if 'save time series' in config:
            self.save_time_series = bool(config['save time series'])

    @staticmethod
    @abstractmethod
    def type():
        pass

    @abstractmethod
    def execute(self, parameters: Parameters, steady_state_values: SystemState) -> DynamicResults:
        """Executes the test and returns the results."""

    def compute(self, parameters: Parameters, steady_state_values: SystemState) -> DynamicResults:
        """Executes the test and returns the results after setting the local logger."""
        local.logger = ParametersLoggerAdapter(logging.getLogger('xWASP_CN'), parameters.dict())
        local.logger.push_stack(self.test_name)
        try:
            return self.execute(parameters, steady_state_values)
        finally:
            local.logger.pop_stack()

    # @abstractmethod
    # def plot_results(self):
    #     """Plots the results of the test in a manner adequate to the test."""

    # def get_radial_results(self,ship_speed,wind_speed,address=[]):
    #     if not ship_speed in self.ship_speeds:
    #         raise InputError('ship_speed',ship_speed,'The requested ship speed is not computed by this test instance')
    #     elif not wind_speed in self.wind_speeds:
    #         raise InputError('wind_speed',wind_speed,'The requested wind speed is not computed by this test instance')
    #     res=[]
    #     for wd in self.wind_directions:
    #         res.append(deepcopy(recursive_access_in_dict(self.results[Parameters(ship_speed,wind_speed,wd)],address)))
    #     return res

    # def plot_standard_polar_results(self,title,radial_label,address=[],factor=1.):
    #     wd_radians=np.radians(self.wind_directions)
    #     figs=[]
    #     for ss in self.ship_speeds:
    #         fig=plt.figure()
    #         ax=fig.add_subplot(projection='polar',label=str(ss))
    #         ax.set_theta_direction(-1)
    #         if max(wd_radians)>np.pi:
    #             ax.set_xticks(np.radians(range(0,360,30)))
    #             ax.set_thetalim(0,2*np.pi)
    #         else:
    #             ax.set_xticks(np.radians(range(0,181,30)))
    #             ax.set_thetalim(0,np.pi)
    #         ax.set_theta_zero_location("N")
    #         for ws in self.wind_speeds:
    #             data = float(factor)*np.array(self.get_radial_results(ss,ws,address))
    #             ax.plot(wd_radians,data,label=str(ws)+' '+self.units['wind speed'],marker='o')
    #         ax.legend(title='Wind speed')
    #         ax.set_ylabel(str(radial_label))
    #         ax.set_title(str(title)+' for Vs = '+str(ss)+self.units['ship speed'])
    #         ax.autoscale(axis='y', tight=True)
    #         figs.append(fig)
    #     return figs

## InputError-raising instantiation of the test classes
def instantiate_test(test_type, name ,*args, **kwargs) -> DynamicComputer:
    try:
        local.logger.push_stack(name)
        return test_type(*args, **kwargs)
    except KeyError as error:
        raise MissingParameter(str(error), 'test: '+name) from error
    except ValueError as error:
        raise InvalidInputError('test', name, str(error)) from  error
    finally:
        local.logger.pop_stack()

# Definition of the tests as classes

class ManoeuvringTest(DynamicComputer):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        self.max_time = float(test['max time'])
        try:
            self.amplitude = get_as_radians(test['amplitude'])
        except InvalidInputError as e:
            raise InvalidInputError('test: amplitude: unit',test['amplitude']['unit'],"Invalid unit for the amplitude of the "+self.type()+" manoeuvring test '"+self.test_name+"': it must be either 'deg' or 'rad'") from e

        if self.heading_controller is not None:
            local.logger.warning("Using a heading controller in a maneuvering computation is strongly discouraged. An actual rudder model (and associated steering command) should be used instead.")

        self.offset = True
        if 'offset command from statics' in test:
            self.offset = bool(test['offset command from statics'])

class ZigZagTest(ManoeuvringTest):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        self.angle_unit = test['amplitude']['unit']

    @staticmethod
    def type():
        return "zig-zag"

    def execute(self, parameters: Parameters, steady_state_values: SystemState):
        model_input = get_modified_yaml(self.yaml, parameters, steady_state_values.states, wind_model=self.wind_model)

        commands = {}
        if self.engine_commands is not None:
            for command in self.engine_commands:
                commands[command] = steady_state_values.commands[command]
        elif self.mode==Mode.PPP:
            F_prop = -steady_state_values.forces[ForceLabel('Fx', 'sum of forces', self.ship_name, self.ship_name)]
            local.logger.debug('Using constant propulsion force Fx=%fN', F_prop)
            add_constant_propulsion_force(model_input, F_prop)
        if self.heading_controller is not None:
            add_simple_heading_controller(model_input, self.heading_controller['T'], self.heading_controller['ksi'], False)
            for command in self.steering_commands:
                commands[command] = 0.
            offset = 0.
        else: # If there is an actual steering model
            for command in self.steering_commands:
                commands[command] = steady_state_values.commands[command]
            offset = steady_state_values.commands[first(self.steering_commands)]

        if not self.offset:
            offset = 0.
        res = {}

        with xdyn.Xdyn_server_stateful(model_input, self.config, self.input_path, output_all_forces=self.save_time_series, signals=commands) as live:

            cond1 = lambda history: history('psi')[-1]<-self.amplitude
            cond2 = lambda history: history('psi')[-1]>self.amplitude

            sign = -1
            conv = True
            turn_counter = 0
            while conv and turn_counter<4:
                if sign<0:
                    cond = [cond1]
                    sign = 1
                else:
                    cond = [cond2]
                    sign = -1
                for command in self.steering_commands:
                    live.set_signal(command, offset + sign*self.amplitude)
                conv=live.run_until_conditions(cond,self.max_time)
                turn_counter = turn_counter + 1

            if not conv:
                local.logger.error("The ship did not reach the target yaw angle in time.")

            # TODO: compute scalar data (turning time, overshoot angles, etc...)

            res['final time'] = UnitValue(live.get_time(), 's')

            time_series = None
            if self.save_time_series:
                time_series = live.get_all()

        return DynamicResults(self.test_name, self.type(), res, time_series)

    # def plot_results(self):
    #     figs=[]
    #     for ss in self.ship_speeds:
    #         for ws in self.wind_speeds:
    #             color_cycle=plt.rcParams['axes.prop_cycle']()
    #             fig,ax=plt.subplots()
    #             for wd in self.wind_directions:
    #                 color=next(color_cycle)
    #                 ax.plot(self.results[Parameters(ss,ws,wd)]['t'],self.results[Parameters(ss,ws,wd)]['rudder'],linestyle='dashed',label=None,**color)
    #                 ax.plot(self.results[Parameters(ss,ws,wd)]['t'],self.results[Parameters(ss,ws,wd)]['yaw'],label=str(wd)+self.units['wind angle'],**color)
    #             ax.legend(title='Wind direction')
    #             ax.set_xlabel('t (s)')
    #             ax.set_ylabel('angle ('+self.angle_unit+')')
    #             ax.grid()
    #             ax.set_title('Rudder command and yaw angle for Vs = '+str(ss)+str(self.units['ship speed'])+' and TWS = '+str(ws)+str(self.units['wind speed']))
    #             figs.append(fig)
    #     return figs


class TurningCircleTest(ManoeuvringTest):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        if 'Lpp' in test:
            self.Lpp=test['Lpp']
        else:
            self.Lpp=None

    @staticmethod
    def type():
        return "turning circle"

    def execute(self, parameters: Parameters, steady_state_values: SystemState):
        model_input = get_modified_yaml(self.yaml, parameters, steady_state_values.states, wind_model=self.wind_model)

        commands = {}
        if self.engine_commands is not None:
            for command in self.engine_commands:
                commands[command] = steady_state_values.commands[command]
        elif self.mode==Mode.PPP:
            F_prop = -steady_state_values.forces[ForceLabel('Fx', 'sum of forces', self.ship_name, self.ship_name)]
            local.logger.debug('Using constant propulsion force Fx=%fN', F_prop)
            add_constant_propulsion_force(model_input, F_prop)
        if self.heading_controller is not None:
            add_simple_heading_controller(model_input, self.heading_controller['T'], self.heading_controller['ksi'], False)
            for command in self.steering_commands:
                commands[command] = self.amplitude
        else: # If there is an actual steering model
            for command in self.steering_commands:
                if self.offset:
                    offset = steady_state_values.commands[command]
                else:
                    offset = 0.
                commands[command] = offset + self.amplitude
        res={}

        with xdyn.Xdyn_server_stateful(model_input, self.config, self.input_path, output_all_forces=self.save_time_series, signals=commands) as live:
            N = int(live.Dt/self.config['dt'])
            local.logger.info("N = %i", N)
            def cond(history):
                return (history('psi')[-N-1]*history('psi')[-1]<0 and abs(history('psi')[-N-1]-history('psi')[-1])>np.pi/2)
            conv=live.run_until_conditions([cond], self.max_time, check_first=False)
            local.logger.debug("Time after first half-turn = %fs", live.get_time())

            if conv:
                max_time=self.max_time-live.get_time()
                conv=live.run_until_conditions([cond], max_time, check_first=False)
                local.logger.debug("Time after second half-turn = %fs", live.get_time())
            if conv:
                max_time=self.max_time-live.get_time()
                conv=live.run_until_conditions([cond], max_time, check_first=False)
                local.logger.debug("Time after third half-turn = %fs", live.get_time())
            if not conv:
                local.logger.error("The ship did not do a turning circle in time.")

            # TODO: compute scalar data (advance, transfer, tactical diameter, etc...)

            res['final time'] = UnitValue(live.get_time(), 's')

            time_series = None
            if self.save_time_series:
                time_series = live.get_all()

        return DynamicResults(self.test_name, self.type(), res, time_series)

    # def plot_results(self):
    #     if self.Lpp:
    #         x='x/Lpp'
    #         y='y/Lpp'
    #     else:
    #         x='x'
    #         y='y'

    #     figs=[]
    #     for ss in self.ship_speeds:
    #         for ws in self.wind_speeds:
    #             color_cycle=plt.rcParams['axes.prop_cycle']()
    #             fig,ax=plt.subplots()
    #             for wd in self.wind_directions:
    #                 color=next(color_cycle)
    #                 ax.plot(self.results[Parameters(ss,ws,wd)][y],self.results[Parameters(ss,ws,wd)][x],label=str(wd)+' '+self.units['wind angle'],**color)
    #             ax.legend(title='Wind direction')
    #             ax.grid()
    #             ax.set_aspect('equal')
    #             ax.set_xlabel(x)
    #             ax.set_ylabel(y)
    #             ax.set_title('Ship trajectory for Vs = '+str(ss)+str(self.units['ship speed'])+' and TWS = '+str(ws)+str(self.units['wind speed']))
    #             figs.append(fig)
    #     return figs

class SeaKeepingTest(DynamicComputer):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        self.T = float(test['T'])
        self.H = valid_UV(test['H'],'test: H')
        self.waves_direction = valid_UV(test['waves direction'],'test: waves direction')
        self.depth = valid_UV(test['depth'],'test: depth')
        self.stretching = {'delta': float(test['stretching']), 'h': self.depth}
        self.fixed_propulsion = bool(test['fixed propulsion']) if 'fixed propulsion' in test else False
        self.fixed_steering = bool(test['fixed steering command']) if 'fixed steering command' in test else False
        self.angle_unit = parse_angle_unit(test['output angle unit']) if 'output angle unit' in test else 'rad'


class RegularWavesTest(SeaKeepingTest):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        self.max_time=float(test['max time'])

        omega = 2*np.pi/self.T
        discretization = {'n': 1,'omega min': {'value': omega, 'unit': 'rad/s'},'omega max': {'value': omega, 'unit': 'rad/s'},'energy fraction': 0.999}
        directional_spreading = {'type': 'dirac','waves propagating to': self.waves_direction}
        spectral_density = {'type': 'dirac', 'omega0': {'value': omega, 'unit': 'rad/s'}, 'Hs': self.H}
        spectra=[{'model': 'airy','depth': self.depth,'seed of the random data generator': 0,'stretching':self.stretching,'directional spreading':directional_spreading,'spectral density':spectral_density}]
        self.waves_model={'model':'waves', 'discretization':discretization, 'spectra': spectra}

        self.forces_target = float(test['mean value target for the sum of']['forces'])
        self.moments_target = float(test['mean value target for the sum of']['moments'])

        self.N = int(10*self.T/self.config['dt'])
        self.cond = []
        self._necessary_output = set()
        def make_cond(DoF, target):
            label = ForceLabel(DoF, 'sum of forces', self.ship_name, self.ship_name).as_xdyn()
            self._necessary_output.add(label)
            return lambda history: abs(np.mean(history(label)[-self.N:]))<target
        for DoF in ['Fx','Fy','Fz']:
            self.cond.append(make_cond(DoF, self.forces_target))
        for DoF in ['Mx','My','Mz']:
            self.cond.append(make_cond(DoF, self.moments_target))

    @staticmethod
    def type():
        return "regular waves"

    def execute(self, parameters: Parameters, steady_state_values: SystemState):
        model_input = get_modified_yaml(self.yaml, parameters, steady_state_values.states, waves=self.waves_model, wind_model=self.wind_model)

        commands={}
        if self.fixed_propulsion:
            if self.engine_commands is not None:
                for command in self.engine_commands:
                    commands[command] = steady_state_values.commands[command]
            elif self.mode == Mode.PPP:
                add_constant_propulsion_force(model_input, -steady_state_values.forces[ForceLabel('Fx', 'sum of forces', self.ship_name, self.ship_name)])

        if self.fixed_steering:
            if self.heading_controller is not None:
                add_simple_heading_controller(model_input, self.heading_controller['T'], self.heading_controller['ksi'], False)
                for command in self.steering_commands:
                    commands[command] = 0.
            else: # If there is an actual steering model
                for command in self.steering_commands:
                    commands[command] = steady_state_values.commands[command]

        res={}

        with xdyn.Xdyn_server_stateful(model_input, self.config, self.input_path, signals=commands, output_all_forces=self.save_time_series, requested_output=self._necessary_output) as live:
            live.simulate(10*self.T)
            conv=live.run_until_conditions(self.cond, self.max_time)

            if not conv:
#                raise ConvergenceError('The wave test simulation failed to converge.',parameters,conv)
                local.logger.error(str(ConvergenceError('The wave test simulation failed to converge to a steady state.',parameters,conv)))

            # TODO: compute scalar data (RAOs, etc...)

            res['final time'] = UnitValue(live.get_time(), 's')

            # factor = 1. if self.angle_unit=='rad' else 180/np.pi
            # time = np.array(live.get_history('t')[-self.N:])
            # var = np.array(live.get_history('x')[-self.N:])
            # var_harmonic = var-var[0]-(time-time[0])*((var[-1]-var[0])/(time[-1]-time[0]))
            # res['surge'] = max(var_harmonic)-min(var_harmonic)
            # var = np.array(live.get_history('y')[-self.N:])
            # var_harmonic = var-var[0]-(time-time[0])*((var[-1]-var[0])/(time[-1]-time[0]))
            # res['sway'] = max(var_harmonic)-min(var_harmonic)
            # var = np.array(live.get_history('z')[-self.N:])
            # var_harmonic = var-var[0]-(time-time[0])*((var[-1]-var[0])/(time[-1]-time[0]))
            # res['heave'] = max(var_harmonic)-min(var_harmonic)
            # var = np.array(live.get_history('phi')[-self.N:])
            # res['roll'] = factor*(max(var)-min(var))
            # var = np.array(live.get_history('theta')[-self.N:])
            # res['pitch'] = factor*(max(var)-min(var))
            # var = np.array(live.get_history('psi')[-self.N:])
            # res['yaw'] = factor*(max(var)-min(var))
            # res['angle_unit'] = self.angle_unit

            time_series = None
            if self.save_time_series:
                time_series = live.get_all()

        return DynamicResults(self.test_name, self.type(), res, time_series)

    # def plot_results(self):
    #     wd_radians=np.radians(self.wind_directions)
    #     figs=[]
    #     for ss in self.ship_speeds:
    #         for ws in self.wind_speeds:
    #             # Plotting surge, sway and heave amplitudes
    #             fig=plt.figure()
    #             ax=fig.add_subplot(projection='polar',label=str(ss))
    #             ax.set_theta_direction(-1)
    #             if max(wd_radians)>np.pi:
    #                 ax.set_xticks(np.radians(range(0,360,30)))
    #                 ax.set_thetalim(0,2*np.pi)
    #             else:
    #                 ax.set_xticks(np.radians(range(0,181,30)))
    #                 ax.set_thetalim(0,np.pi)
    #             ax.set_theta_zero_location("N")
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['surge']),label='surge',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['sway']),label='sway',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['heave']),label='heave',marker='o')
    #             ax.set_ylabel('m')
    #             ax.legend(title='DoF')
    #             ax.set_title('Response amplitudes for Vs = '+str(ss)+self.units['ship speed']+', TWS = '+str(ws)+' '+self.units['wind speed']+', T = '+str(self.T)+'s and H = '+str(self.H['value'])+self.H['unit'])
    #             figs.append(fig)
    #             # Plotting roll, pitch and yaw amplitudes
    #             fig=plt.figure()
    #             ax=fig.add_subplot(projection='polar',label=str(ss))
    #             ax.set_theta_direction(-1)
    #             if max(wd_radians)>np.pi:
    #                 ax.set_xticks(np.radians(range(0,360,30)))
    #                 ax.set_thetalim(0,2*np.pi)
    #             else:
    #                 ax.set_xticks(np.radians(range(0,181,30)))
    #                 ax.set_thetalim(0,np.pi)
    #             ax.set_theta_zero_location("N")
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['roll']),label='roll',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['pitch']),label='pitch',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['yaw']),label='yaw',marker='o')
    #             ax.set_ylabel(self.angle_unit)
    #             ax.legend(title='DoF')
    #             ax.set_title('Response amplitudes for Vs = '+str(ss)+self.units['ship speed']+', TWS = '+str(ws)+' '+self.units['wind speed']+', T = '+str(self.T)+'s and H = '+str(self.H['value'])+self.H['unit'])
    #             figs.append(fig)
    #     return figs

class SpectralWavesTest(SeaKeepingTest):
    def __init__(self, input_file, config, test, mode: Mode, engine_commands: Set[str] = None, steering_commands: str = None, wind_model: dict = None):
        super().__init__(input_file, config, test, mode, engine_commands, steering_commands, wind_model)
        self.depth = valid_UV(test['depth'],'test: depth')
        self.spectrum = str(test['spectrum']).lower()
        spectral_density = {'type': self.spectrum,
                            'Tp': {'value': self.T, 'unit': 'rad/s'},
                            'Hs': self.H}
        if self.spectrum=='jonswap':
            if 'gamma' in test:
                spectral_density['gamma'] = test['gamma']
            else:
                raise InvalidInputError('test: gamma',test,"Parameter 'gamma' is required for a JONSWAP sprectrum but was not found")

        self.directional_spreading = bool(test['directional spreading']) if 'directional spreading' in test else False
        directional_spreading = {'type': 'dirac',
                                 'waves propagating to': self.waves_direction}
        if self.directional_spreading:
            directional_spreading['type'] = 'cos2s'
            directional_spreading['s'] = test['s']
        else:
            local.logger.info('Defaulting to Dirac directional spectrum (monodirectional waves)')
            directional_spreading['type'] = 'dirac'

        self.discretization = int(test['number of periods'])
        self.pulsation_bounds = (2*np.pi/float(test['period bounds']['max']),2*np.pi/float(test['period bounds']['min']))
        discretization = {'n': self.discretization,
                          'omega min': {'value': self.pulsation_bounds[0], 'unit': 'rad/s'},
                          'omega max': {'value': self.pulsation_bounds[1], 'unit': 'rad/s'},
                          'energy fraction': 0.999}

        spectra=[{'model': 'airy',
                  'depth': self.depth,
                  'seed of the random data generator': random.randint(0,1000000),
                  'stretching': self.stretching, 'directional spreading': directional_spreading,
                  'spectral density':spectral_density}]
        self.waves_model={'model': 'waves',
                          'discretization': discretization,
                          'spectra': spectra}

        self.simulation_time = float(test['simulation time'])

    @staticmethod
    def type():
        return "spectral waves"

    def execute(self, parameters: Parameters, steady_state_values: SystemState):
        model_input = get_modified_yaml(self.yaml, parameters, steady_state_values.states, waves=self.waves_model, wind_model=self.wind_model)

        commands={}
        if self.fixed_propulsion:
            if self.engine_commands is not None:
                for command in self.engine_commands:
                    commands[command] = steady_state_values.commands[command]
            elif self.mode == Mode.PPP:
                add_constant_propulsion_force(model_input, -steady_state_values.forces[ForceLabel('Fx', 'sum of forces', self.ship_name, self.ship_name)])

        if self.fixed_steering:
            if self.heading_controller is not None:
                add_simple_heading_controller(model_input, self.heading_controller['T'], self.heading_controller['ksi'], False)
                for command in self.steering_commands:
                    commands[command] = 0.
            else: # If there is an actual steering model
                for command in self.steering_commands:
                    commands[command] = steady_state_values.commands[command]

        res={}

        with xdyn.Xdyn_server_stateful(model_input, self.config, self.input_path, signals=commands, output_all_forces=self.save_time_series) as live:
            live.simulate(self.simulation_time)

            # TODO: compute scalar data (turning time, overshoot angles, etc...)

            res['final time'] = UnitValue(live.get_time(), 's')

            # factor = 1. if self.angle_unit=='rad' else 180/np.pi
            # x_harmonic = np.array(live.get_history('x'))-np.array(live.get_history('u'))*np.array(live.get_history('t'))-live.get_history('x')[0]
            # res['surge'] = max(x_harmonic)-min(x_harmonic)
            # y_harmonic = np.array(live.get_history('y'))-np.array(live.get_history('v'))*np.array(live.get_history('t'))-live.get_history('y')[0]
            # res['sway'] = max(y_harmonic)-min(y_harmonic)
            # z_harmonic = np.array(live.get_history('z'))-np.array(live.get_history('w'))*np.array(live.get_history('t'))-live.get_history('z')[0]
            # res['heave'] = max(z_harmonic)-min(z_harmonic)
            # res['roll'] = factor*(max(live.get_history('phi'))-min(live.get_history('phi')))
            # res['pitch'] = factor*(max(live.get_history('theta'))-min(live.get_history('theta')))
            # res['yaw'] = factor*(max(live.get_history('psi'))-min(live.get_history('psi')))
            # res['angle_unit'] = self.angle_unit

            time_series = None
            if self.save_time_series:
                time_series = live.get_all()

        return DynamicResults(self.test_name, self.type(), res, time_series)

    # def plot_results(self):
    #     wd_radians=np.radians(self.wind_directions)
    #     figs=[]
    #     for ss in self.ship_speeds:
    #         for ws in self.wind_speeds:
    #             # Plotting surge, sway and heave amplitudes
    #             fig=plt.figure()
    #             ax=fig.add_subplot(projection='polar',label=str(ss))
    #             ax.set_theta_direction(-1)
    #             if max(wd_radians)>np.pi:
    #                 ax.set_xticks(np.radians(range(0,360,30)))
    #                 ax.set_thetalim(0,2*np.pi)
    #             else:
    #                 ax.set_xticks(np.radians(range(0,181,30)))
    #                 ax.set_thetalim(0,np.pi)
    #             ax.set_theta_zero_location("N")
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['surge']),label='surge',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['sway']),label='sway',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['heave']),label='heave',marker='o')
    #             ax.set_ylabel('m')
    #             ax.legend(title='DoF')
    #             ax.set_title('Response amplitudes for Vs = '+str(ss)+self.units['ship speed']+', TWS = '+str(ws)+' '+self.units['wind speed']+', Tp = '+str(self.peak_period)+'s and Hs = '+str(self.significant_height['value'])+self.significant_height['unit'])
    #             figs.append(fig)
    #             # Plotting roll, pitch and yaw amplitudes
    #             fig=plt.figure()
    #             ax=fig.add_subplot(projection='polar',label=str(ss))
    #             ax.set_theta_direction(-1)
    #             if max(wd_radians)>np.pi:
    #                 ax.set_xticks(np.radians(range(0,360,30)))
    #                 ax.set_thetalim(0,2*np.pi)
    #             else:
    #                 ax.set_xticks(np.radians(range(0,181,30)))
    #                 ax.set_thetalim(0,np.pi)
    #             ax.set_theta_zero_location("N")
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['roll']),label='roll',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['pitch']),label='pitch',marker='o')
    #             ax.plot(wd_radians,self.get_radial_results(ss,ws,['yaw']),label='yaw',marker='o')
    #             ax.set_ylabel(self.angle_unit)
    #             ax.legend(title='DoF')
    #             ax.set_title('Response amplitudes for Vs = '+str(ss)+self.units['ship speed']+', TWS = '+str(ws)+' '+self.units['wind speed']+', Tp = '+str(self.peak_period)+'s and Hs = '+str(self.significant_height['value'])+self.significant_height['unit'])
    #             figs.append(fig)
    #     return figs

def recursive_access_in_dict(data,address):
    if len(address)==0:
        return data
    elif address[0] in data:
        return recursive_access_in_dict(data[address[0]],address[1:])
    else:
        raise InvalidInputError('address',address,"No field corresponding to the address was found in dict 'data'")
