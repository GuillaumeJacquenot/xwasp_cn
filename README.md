xWASP_CN is a 6-DoFs Dynamic Velocity/Power Prediction Program dedicated to Wind Assisted Ship Propulsion. See [the wiki](https://gitlab.com/lheea/xwasp_cn/-/wikis/home) for further details.

It relies heavily on [`xdyn`](https://gitlab.com/sirehna_naval_group/sirehna/xdyn), a ship simulator developed by [Sirehna](https://www.sirehna.com/).

xWASP_CN was developed at the LHEEA (Ecole Centrale de Nantes/CNRS) with financial support from the French Energy Agency (ADEME) and the Région Pays de la Loire.

<table>
<tr>
<td>
<a href="https://lheea.ec-nantes.fr/english-version">
<img src="logos/LogoCN_LHEEA_CNRS_fit.svg"
     alt="Logo LHEEA"
     height=90>
</a>
</td>
<td>
<a href="https://www.ademe.fr/en" style="text-align: center">
<img src="logos/ADEME_logo_2020.svg"
     alt="Logo ADEME"
     height=90
     align="center">
</a>
</td>
<td>
<a href="https://www.paysdelaloire.fr/">
<img src="logos/RVB-PDL_Institutionnel.svg"
     alt="Logo Region Pays de la Loire"
     height=90>
</a>
</td>
</tr>
</table>
