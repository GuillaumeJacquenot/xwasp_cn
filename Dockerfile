# This image is based on the latest release of xdyn for building xWASP_CN

FROM sirehna/xdyn:v6-0-3

RUN apt-get update -yq && \
    apt-get install \
        --yes \
        --no-install-recommends \
        python3 \
        python3-pip && \
    apt-get autoclean && \
    apt-get autoremove && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* && \
    rm -rf /var/lib/apt/lists

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install grpcio-tools

ENV GRPC_ENABLE_FORK_SUPPORT 0

# Adding protobuf files to communicate with xdyn
ADD https://gitlab.com/api/v4/projects/35688670/repository/files/cosimulation.proto/raw?ref=master /proto/cosimulation.proto
ADD https://gitlab.com/api/v4/projects/35688670/repository/files/model_exchange.proto/raw?ref=master /proto/model_exchange.proto

RUN cd /proto && \
    python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. cosimulation.proto && \
    python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. model_exchange.proto

ENV PYTHONPATH "${PYTHONPATH}:/proto"

COPY . /opt/xWASP_CN

ARG GIT_HEAD_HASH=missing_hash

RUN sed -i "s/latest_commit_id_placeholder/$GIT_HEAD_HASH/" /opt/xWASP_CN/xWASP_CN/__main__.py

RUN python3 -m pip install /opt/xWASP_CN --compile && \
    rm -rf /opt/xWASP_CN

VOLUME /work

WORKDIR /work

ENTRYPOINT ["xWASP_CN"]

